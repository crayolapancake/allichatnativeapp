import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import Link from './Link';
import Links from './Links';
import LinkElement from './LinkElement';
import LinksStepContainer from './LinksStepContainer';
import LinkDescription from './LinkDescription';
import LinkLabel from './LinkLabel';
import Thinking from '../Thinking';

class LinksStep extends Component {
  /* istanbul ignore next */
  constructor(props) {
    super(props);

    this.renderLink = this.renderLink.bind(this);
  }

  renderLink(link) {
    const { label, href, summary } = link;

    return (

      <Link
        key={href}
        className="rsc-ls-link"
      >
        <LinkElement
          className="rsc-ls-link-element"
          href={href}
          role="link"
          onClick={this.props.action}
        >
          <LinkLabel
            className="rsc-ls-link-label"
          >
          {label}
          </LinkLabel>
          {summary && (
          <LinkDescription
            className="rsc-ls-link-desc"
            role=""
          >
            {summary}
          </LinkDescription>
          )}
        </LinkElement>
      </Link>
    );
  }

  render() {
    const { links } = this.props.step;

    return (
      <LinksStepContainer
        className="rsc-ls"
        roll="scrollbar">
        <Links className="rsc-ls-links">
          {_.map(links, this.renderLink)}
        </Links>
      </LinksStepContainer>
    );
  }
}

LinksStep.propTypes = {
  step: PropTypes.object.isRequired,
};

export default LinksStep;
