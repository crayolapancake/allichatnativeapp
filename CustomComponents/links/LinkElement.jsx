import styled from 'styled-components';
import { isMobile } from 'react-device-detect';
import defaultTheme from '../../theme';

const LinkElement = styled.a`
  display: block;
  background: ${({ theme }) => theme.linkContainerBackgroundColor};
  border-radius: 5px;
  color: ${({ theme }) => theme.botFontColor};
  font-size: ${ isMobile ? ({ theme }) => theme.optionFontSizeMobile : ({ theme }) => theme.optionFontSizeBrowser};
  line-height: ${({ theme }) => theme.lineheight};
  margin: 10px 0px 5px 0px;
  padding: 0px 0px 0px 10px;
  border-left: ${({ theme }) => theme.linkBorderColor} solid 2px;
  text-decoration: none;
  box-shadow: 2px 2px 2px 0px #A0A0A060;
  cursor: pointer;
  &:hover { opacity: .7; }
  opacity: none;
`;

LinkElement.defaultProps = {
  theme: defaultTheme,
};

export default LinkElement;
