import styled from 'styled-components';

const Links = styled.ul`
  margin: 2px 0px 10px 2px;
  padding: 0px 0px 10px 10px;
  text-align: left;
  list-style-type: none;
  display: block;
  opacity: none;
  `;

export default Links;
