import styled from 'styled-components';
import defaultTheme from '../../theme';

const LinkLabel = styled.div`
  background: ${({ theme }) => theme.linkContainerBackgroundColor};
  color: ${({ theme }) => theme.linkLabel};
  font-size: ${({ theme }) => theme.linkLabelFontSize}
  font-weight: bold;
  padding: 5px 5px 5px 0px;
  border-radius: 5px;
  margin: 0px;
  max-width: 480px;
  text-align: left;
  white-space: normal;
  display: block;
  text-decoration: none;
  display: list-item;
  opacity: none;
  `;

LinkLabel.defaultProps = {
  theme: defaultTheme,
};

export default LinkLabel;
