import styled from 'styled-components';
import { scale } from '../../common/animations';

const Link = styled.li`
  animation: ${scale} .3s ease forwards;
  cursor: pointer;
  margin: 8px, 2px;
  padding-top: 1px;
  transform: scale(0);
  text-align: -webkit-match-parent;
  opacity: none;
`;

export default Link;
