import styled from 'styled-components';
import defaultTheme from '../../theme';

const LinkDescription = styled.div`
  background: ${({ theme }) => theme.linkContainerBackgroundColor};
  color: ${({ theme }) => theme.linkTextFontColor};
  font-size: ${({ theme }) => theme.linkDescFontSize};
  font-family: ${({ theme }) => theme.linkDescFontFamily};
  font-weight: 500;
  padding: 5px 5px 5px 0px;
  max-width: 95%;
  text-align: left;
  white-space: normal;
  margin: 0px;
  display: inherit;
  text-decoration: none;
  opacity: none;
`;

LinkDescription.defaultProps = {
  theme: defaultTheme,
};

export default LinkDescription;
