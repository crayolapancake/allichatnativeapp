import React, { Component } from 'react';
import { StyleSheet, Text, View, Linking } from 'react-native';


class LinkAnxiety extends Component {
  render() {
    return (
      <View style={styles.LinkContainer}>
        <Text style={styles.TextStyle} onPress={ () => Linking.openURL('https://youngminds.org.uk/blog/coping-with-anxiety-at-university/')}
        >Coping with Anxiety (Click to Read More)
        </Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({

  LinkContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#e2daee',
    margin: 0,
    padding: 0,
  },

  TextStyle: {
    color: '#4a4a4a',
    textDecorationLine: 'underline',
    fontWeight: 'bold',
    fontSize: 18,
  }
});



export default LinkAnxiety;
