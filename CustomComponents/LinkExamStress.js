import React, { Component } from 'react';
import { StyleSheet, Text, View, Linking } from 'react-native';


class LinkExamStress extends Component {
  render() {
    return (
      <View style={styles.LinkContainer}>
        <Text style={styles.TextStyle} onPress={ () => Linking.openURL('https://www.childline.org.uk/info-advice/school-college-and-work/school-college/exam-stress/')}
        >Coping with Exam Stress (Click to Read More)
        </Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({

  LinkContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#e2daee',
    margin: 0,
    padding: 0,
  },

  TextStyle: {
    color: '#4a4a4a',
    textDecorationLine: 'underline',
    fontWeight: 'bold',
    fontSize: 18,
  }
});



export default LinkExamStress;
