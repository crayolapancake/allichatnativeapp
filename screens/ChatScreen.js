import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import ChatBot from 'react-native-chatbot';
import LinkAnxiety from '../CustomComponents/LinkAnxiety';
import LinkExamStress from '../CustomComponents/LinkExamStress';
import GestureRecognizer, {swipeDirections} from 'react-native-swipe-gestures';

const steps = [
  // greeting
  {
    id: '0',
    message: 'Hello! I\'m Alli! I\'m a catbot who can help with topics like anxiety and exam stress. ✍️ Soon, I\'ll be able to help with more!',
    delay: 1000,
    trigger: '0.1',
  },
  {
    id: '0.1',
    message: 'Our chat is not intended to be a medical intervention or replacement for in-person therapy. In an emergency, please call 999.',
    delay: 3000,
    trigger: '0.2',
  },
  {
    id: '0.2',
    options: [
      { value: 1, label: 'I understand', trigger: '0.3' },
      { value: 2, label: 'I don\'t agree', trigger: '9.9' },
    ],
  },
  {
    id: '0.3',
    message: 'Great! What\'s your name?',
    delay: 2000,
    trigger: '2',
  },
  //capture name
  {
    id: '2',
    user: true,
    inputAttributes: {
      keyboardType: 'default'
    },
    trigger: '3',
    end: false,
  },
  {
    id: '3',
    message: ({ previousValue, steps }) => 'Great to meet you {previousValue}.',
    delay: 2000,
    trigger: '4',
  },
  // send OptionsStep
  {
    id: '4',
    message: 'Please choose what you\'d like information about.',
    delay: 2000,
    trigger: '5',
  },

  {
    id: '5',
    options: [
      { value: 1, label: 'Anxiety', trigger: '6' },
      { value: 2, label: 'Exam Stress', trigger: '7' },
    ],
  },
  {
    id: '6',
    message: 'Let\'s see what I can find on anxiety...',
    delay: 2000,
    trigger: '8',
  },
  {
    id: '8',
    component: <LinkAnxiety />,
    trigger: '10'
  },
  {
    id: '7',
    message: 'Let\'s see what I can find on exam stress...',
    delay: 2000,
    trigger: '9',
  },
  {
    id: '9',
    component: <LinkExamStress />,
    trigger: '10',
  },
  {
    id: '9.9',
    message: 'Sorry, you must agree to the terms to use Allichat.',
    delay: 2000,
    trigger: '13',
  },
  {
    id: '10',
    message: ' Did you find that helpful?',
    delay: 4000,
    trigger: '11',
  },
  {
    id: '11',
    options: [
      { value: 1, label: 'Yes', trigger: '12' },
      { value: 2, label: 'No', trigger: '12' },
    ],
  },
  {
    id: '12',
    message: 'Thank you for your feedback. Bye for now!',
    end: true,
  },
  {
    id: '13',
    message: 'Bye for now!',
    end: true,
  },
];

class ChatScreen extends Component {

  onSwipe(gestureName, gestureState) {
   const {SWIPE_UP, SWIPE_DOWN, SWIPE_LEFT, SWIPE_RIGHT} = swipeDirections;
   const {navigate} = this.props.navigation;

   this.setState({gestureName: gestureName});
   switch (gestureName) {
     case SWIPE_UP:
     alert("up")
       break;
     case SWIPE_DOWN:
     alert("down")
       break;
     case SWIPE_LEFT:
     alert("right")
       break;
     case SWIPE_RIGHT:
       navigate('Home')
       break;
   }
 }

  render() {
    return (
      <GestureRecognizer
        onSwipe={(direction, state) => this.onSwipe(direction, state)}
        style={styles.gestureRecognizer}
      >
        <View>
          <ChatBot steps={steps} />
        </View>
      </GestureRecognizer>
    );
  }
}


const styles = StyleSheet.create({
  gestureRecognizer: {
    flex: 1,
  },
});

export default ChatScreen;
