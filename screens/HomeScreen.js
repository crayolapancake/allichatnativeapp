import React, { Component } from 'react';
import LinearGradient from 'react-native-linear-gradient';
import { Button, View, StyleSheet, TouchableOpacity, Image } from 'react-native';
import GestureRecognizer, {swipeDirections} from 'react-native-swipe-gestures';

class HomeScreen extends React.Component {

  static navigationOptions = {
    title: 'AlliChat',
    headerStyle: {
      backgroundColor: '#324b69',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold',
      fontSize: 28,
      justifyContent: 'center',
      alignItems: 'center',
      alignContent: 'center',
    },
  };

  onSwipe(gestureName, gestureState) {
   const {SWIPE_UP, SWIPE_DOWN, SWIPE_LEFT, SWIPE_RIGHT} = swipeDirections;
   const {navigate} = this.props.navigation;

   this.setState({gestureName: gestureName});
   switch (gestureName) {
     case SWIPE_UP:
     alert("up")
       break;
     case SWIPE_DOWN:
     alert("down")
       break;
     case SWIPE_LEFT:
      navigate('Chat')
       break;
     case SWIPE_RIGHT:
      navigate('History')
       break;
   }
 }

  render() {
    const {navigate} = this.props.navigation;
    const gradientColors = ['#324b69', '#3f5e84', '#a1b7d2'];

    const config = {
      velocityThreshold: 0.3,
      directionalOffsetThreshold: 80
    };

    return (
      <GestureRecognizer
        onSwipe={(direction, state) => this.onSwipe(direction, state)}
        style={styles.gestureRecognizer}
        >
        <LinearGradient
          colors={gradientColors}
          style={styles.linearGradient}
        >
          <View style={styles.viewMargin}>
            <TouchableOpacity
              style={styles.picButton}
              onPress={() => navigate('Chat')}
              >
                <Image
                  style={styles.image}
                  source={require('../images/allichat.png')}
                />
              </TouchableOpacity>

            <View style={styles.view}>
              <Button
                title="Speak to AlliChat"
                onPress={() => navigate('Chat')}
                color={styles.button.color}
                accessibilityLabel="Tap me"
              />
            </View>
            <View style={styles.view}>
              <Button
                title="Programs"
                onPress={() => navigate('Programs')}
                color={styles.button.color}
              />
            </View>
            <View style={styles.view}>
              <Button
                title="Your History"
                onPress={() => navigate('History', {ID: 'userIdOrWhatevs'} )}
                color={styles.button.color}
              />
            </View>
            <View style={styles.view}>
              <Button
                title="Your Progress"
                onPress={() => navigate('Progress', {ID: 'userIdOrWhatevs'} )}
                color={styles.button.color}
              />
            </View>
          </View>
        </LinearGradient>
      </GestureRecognizer>
    );
  }
}

const styles = StyleSheet.create({
  gestureRecognizer: {
    flex: 1,
  },
  button: {
    color: '#324b69',
  },
  view: {
    margin: 10,
    justifyContent: 'center',
    alignContent: 'space-between',
  },
  viewMargin: {
    marginTop: '5%',
  },
  picButton: {
    height: 280,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: '5%',
  },
  image: {
    height: 275,
    width: 287,
  },
  linearGradient: {
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 0,
  },
});

export default HomeScreen;
