import React, { Component } from 'react';
import { Button, View, StyleSheet } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';


class ProgramScreen extends React.Component {

  static navigationOptions = {
    title: 'AlliChat Programs',
  };

  render() {
    const {navigate} = this.props.navigation;
    const gradientColors = ['#324b69', '#3f5e84', '#a1b7d2'];

    return (
      <LinearGradient
        colors={gradientColors}
        style={styles.linearGradient}
      >
        <View style={styles.viewMargin}>
          <View style={styles.view}>
            <Button
              title="Breathing Exercises"
              // onPress={() => navigate('Program1')}
              color={styles.button.color}
            />
          </View>
          <View style={styles.view}>
            <Button
              title="Gratitude Journal"
              // onPress={() => navigate('Program2')}
              color={styles.button.color}
            />
          </View>
        </View>
      </LinearGradient>
    );
  }
}

const styles = {
  button: {
    color: '#324b69',
  },
  viewMargin: {
    marginTop: '40%',
  },
  view: {
    margin: 20,
    justifyContent: 'center',
    alignContent: 'space-between',
    // alignItems: 'center'
  },
  linearGradient: {
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 0
  },
};

export default ProgramScreen;
