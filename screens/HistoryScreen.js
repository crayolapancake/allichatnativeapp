import React, { Component } from 'react';
import { Button, View, StyleSheet } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import GestureRecognizer, {swipeDirections} from 'react-native-swipe-gestures';


class HistoryScreen extends React.Component {

  static navigationOptions = {
    title: 'AlliChat Programs',
  };

  onSwipe(gestureName, gestureState) {
   const {SWIPE_UP, SWIPE_DOWN, SWIPE_LEFT, SWIPE_RIGHT} = swipeDirections;
   const {navigate} = this.props.navigation;

   this.setState({gestureName: gestureName});
   switch (gestureName) {
     case SWIPE_UP:
     alert("up")
       break;
     case SWIPE_DOWN:
     alert("down")
       break;
     case SWIPE_LEFT:
      navigate('Home')
       break;
     case SWIPE_RIGHT:
      alert("right")
       break;
   }
 }

  render() {
    const {navigate} = this.props.navigation;
    const gradientColors = ['#324b69', '#3f5e84', '#a1b7d2'];

    return (
      <GestureRecognizer
        onSwipe={(direction, state) => this.onSwipe(direction, state)}
        style={styles.gestureRecognizer}
      >
        <LinearGradient
          colors={gradientColors}
          style={styles.linearGradient}
        >
          <View style={styles.viewMargin}>
            <View style={styles.view}>
              <Button
                title="History 1"
                // onPress={() => navigate('Program1')}
                color={styles.button.color}
              />
            </View>
            <View style={styles.view}>
              <Button
                title="History 2"
                // onPress={() => navigate('Program2')}
                color={styles.button.color}
              />
            </View>
          </View>
        </LinearGradient>
      </GestureRecognizer>
    );
  }
}

const styles = StyleSheet.create({
  gestureRecognizer: {
    flex: 1,
  },
  button: {
    color: '#324b69',
  },
  viewMargin: {
    marginTop: '40%'
  },
  view: {
    margin: 20,
    justifyContent: 'center',
    alignContent: 'space-between',
    // alignItems: 'center'
  },
  linearGradient: {
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 0
  },
});

export default HistoryScreen;
