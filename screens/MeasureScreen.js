import React, { Component } from 'react';
import { Button, View, Text, StyleSheet } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Slider from '@react-native-community/slider';


class MeasureScreen extends React.Component {

  static navigationOptions = {
    title: 'Measure your Progress',
  };

  render() {
    const {navigate} = this.props.navigation;
    const gradientColors = ['#324b69', '#3f5e84', '#a1b7d2'];

    return (
      <LinearGradient
        colors={gradientColors}
        style={styles.linearGradient}
      >
        <View style={styles.topView}>
          <Text style={styles.text}>Happiness</Text>
          <Slider
            style={styles.sliderStyle}
            minimumValue={0}
            maximumValue={10}
            minimumTrackTintColor="#FFFFFF"
            maximumTrackTintColor="#000"
            thumbTintColor="#4fa3a7"
          />
          <Text style={styles.text}>Tiredness</Text>
          <Slider
            style={styles.sliderStyle}
            minimumValue={0}
            maximumValue={10}
            minimumTrackTintColor="#FFFFFF"
            maximumTrackTintColor="#000"
            thumbTintColor="#4fa3a7"
          />
          <Text style={styles.text}>Hunger</Text>
          <Slider
            style={styles.sliderStyle}
            minimumValue={0}
            maximumValue={10}
            minimumTrackTintColor="#FFFFFF"
            maximumTrackTintColor="#000"
            thumbTintColor="#4fa3a7"
          />
        </View>
      </LinearGradient>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    color: '#324b69',
  },
  topView: {
    marginTop: '20%',
    justifyContent: 'center',
    alignContent: 'space-between',
  },
  linearGradient: {
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 0
  },
  sliderStyle :{
    width: 300,
    height: 40,
  },
  text: {
    marginTop: 50,
    color: '#fff',
    justifyContent: 'center',
    fontSize: 24,
  }
});

export default MeasureScreen;
