import {createStackNavigator, createAppContainer} from 'react-navigation';
import ChatScreen from './screens/ChatScreen';
import HomeScreen from './screens/HomeScreen';
import ProgramScreen from './screens/ProgramScreen';
import HistoryScreen from './screens/HistoryScreen';
import MeasureScreen from './screens/MeasureScreen';


const MainNavigator = createStackNavigator({
  Home: {screen: HomeScreen},
  Chat: {screen: ChatScreen},
  Programs: {screen: ProgramScreen},
  History: {screen: HistoryScreen},
  Progress: {screen: MeasureScreen},
});

const App = createAppContainer(MainNavigator);

export default App;
